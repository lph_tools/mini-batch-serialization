[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://bitbucket.org/lph_tools/mini-batch-serialization/src/8414a875b8a69242327b9585401b9e670fc77c52/LICENSE.md?at=master&fileviewer=file-view-default)

# About Mini-Batch Serialization

This repository contains the pytorch codes for training ResNet with Group Normalization using Mini-Batch Serialization (https://arxiv.org/pdf/1810.00307.pdf). We train ResNet50 for 90 epochs using mini-batch size of 128 distributed to four GPUs under different GroupNorm policies. E.g (1) using a fixed number of groups per layer and (2) a fixed number of channels per group. For the experiments, an initial leraning rate of 0.05 is used and decayed at 31, 61 and 81 epochs by 0.1. We compare the training results to Batch Normalization with the same training conditions.

| Model         | Policy                                         | Top1 Accuracy | Link   |
| ------------- |:----------------------------------------------:|:-------------:|:------:|
| ResNet50      | Batch Norm                                     |   76.0%       | [Pre-trained model:BN](https://bitbucket.org/lph_tools/mini-batch-serialization/downloads/checkpoint.bn_90epochs_MiniBatch128_LR005.tar)     |
| ResNet50      | GN(16 channels per group for all layers) + MBS |   76.2%       | [Pre-trained model:GN-CHs](https://bitbucket.org/lph_tools/mini-batch-serialization/downloads/checkpoint.gn_chs_90epochs_MiniBatch128_LR005.tar) |
| ResNet50      | GN(32 groups for all layers) + MBS             |   75.8%       | [Pre-trained model:GN-GRs](https://bitbucket.org/lph_tools/mini-batch-serialization/downloads/checkpoint.gn_grp_90epochs_MiniBatch128_LR005.tar) |


# Train ResNet50

We train using a mini-batch size of 128 distributed to 4 GPUs as the baseline. Please increase the learning rate to 0.1 when using mini-batch size 256.

* Training with MBS + Group Normalization (a fixed number of groups, 32)
```
python imagenet.py --data /imagenet-data/raw-data --learning-rate 0.05 --schedule 31 61 81 --checkpoint /directory/to/store/checkpoint --arch resnet_gn_mbs50 --gpu-id 0,1,2,3 --train_batch 128 --test_batch 128 --gn_type fixed_group --gn_val 32 --sub_batch_sizes 4 8 16 32
```
* Training with MBS + Group Normalization (a fixed number of channels per group, 16)
```
python imagenet.py --data /imagenet-data/raw-data --learning-rate 0.05 --schedule 31 61 81 --checkpoint /directory/to/store/checkpoint --arch resnet_gn_mbs50 --gpu-id 0,1,2,3 --train_batch 128 --test_batch 128 --gn_type fixed_channel --gn_val 16 --sub_batch_sizes 4 8 16 32
```
* Training whth Batch Normalization
```
python imagenet.py --data /imagenet-data/raw-data --learning-rate 0.05 --schedule 31 61 81 --checkpoint /directory/to/store/checkpoint --arch resnet50 --gpu-id 0,1,2,3 --train_batch 128 --test_batch 128 
```

# Evaluation using pre-trained models

* Evaluation with MBS + Group Normalization (a fixed number of groups, 32)
```
python imagenet.py  --data /imagenet-data/raw-data --learning-rate 0.05 --schedule 31 61 81 --resume directory/of/checkpoint --arch resnet_gn_mbs50 --test_batch 32 --gn_type fixed_channels --gn_val 16 --sub_batch_sizes 4 8 16 32 --evaluate
```

* Evaluation with MBS + Group Normalization (a fixed number of channels per group, 16)
```
python imagenet.py  --data /imagenet-data/raw-data --learning-rate 0.05 --schedule 31 61 81 --resume directory/of/checkpoint --arch resnet_gn_mbs50 --test_batch 32 --gn_type fixed_groups --gn_val 32 --sub_batch_sizes 4 8 16 32  --evaluate
```

* Evaluation Batch Normalization
```
python imagenet.py --data /imagenet-data/raw-data --learning-rate 0.05 --schedule 31 61 81 --arch resnet50 --test_batch 32 --resume /directory/of/checkpoint --evaluate
```
