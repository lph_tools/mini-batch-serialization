'''
ResNet model with MBS (Mini-batch Serialization) for training
Copyright (c) Sangkug Lym, The University of Texas at Austin, 2018
'''
import torch
from .resnet_gn import BasicBlock, Bottleneck, ResNetGn

__all__ = ['resnet_gn_mbs18', 'resnet_gn_mbs34', 'resnet_gn_mbs50', 'resnet_gn_mbs101',
           'resnet_gn_mbs152']

class ResNetGnMBS(ResNetGn):
    def __init__(self, block, layers, num_classes=1000, 
            gn_type='fixed_channels', gn_val=16, sub_batch_sizes=[4,8,16,32]):
        super().__init__(block, layers, num_classes, gn_type, gn_val)
        self.sub_batch_sizes = sub_batch_sizes
        #print("sub-batch sizes: {}".format(sub_batch_sizes))

    """ Generate the sub_batch size at each resnet stage  """
    def get_sub_batch_sizes(self, mini_batch):
        split_sizes = []
        for sub_batch_size in self.sub_batch_sizes:
            if mini_batch % sub_batch_size == 0:
                out = [sub_batch_size for i in range(mini_batch // sub_batch_size)]
            else:
                last = mini_batch % sub_batch_size
                left = mini_batch - last
                out = [sub_batch_size for i in range(left // sub_batch_size)]
                out.append(last)
            split_sizes.append(out)
        return split_sizes

    def forward(self, x):
        split_sizes = self.get_sub_batch_sizes(x.shape[0])

        x = list(x.split(split_sizes[0],dim=0))
        for i, _x in enumerate(x):
            _x = self.conv1(_x)
            _x = self.bn1(_x)
            _x = self.relu(_x)
            _x = self.maxpool(_x)
            x[i] = self.layer1(_x)
        x = torch.cat(x, dim=0)

        x = list(x.split(split_sizes[1], dim=0))
        for i in range(len(split_sizes[1])):
            x[i] = self.layer2(x[i])
        x = torch.cat(x, dim=0)

        x = list(x.split(split_sizes[2], dim=0))
        for i in range(len(split_sizes[2])):
            x[i] = self.layer3(x[i])
        x = torch.cat(x, dim=0)

        x = list(x.split(split_sizes[3], dim=0))
        for i in range(len(split_sizes[3])):
            x[i] = self.layer4(x[i])
        x = torch.cat(x, dim=0)

        x = self.avgpool(x)
        x = x.view(x.size(0), -1)
        x = self.fc(x)

        return x


def resnet_gn_mbs18(pretrained=False, **kwargs):
    model = ResNetGnMBS(BasicBlock, [2, 2, 2, 2], **kwargs)
    return model

def resnet_gn_mbs34(pretrained=False, **kwargs):
    model = ResNetGnMBS(BasicBlock, [3, 4, 6, 3], **kwargs)
    return model

def resnet_gn_mbs50(pretrained=False, **kwargs):
    model = ResNetGnMBS(Bottleneck, [3, 4, 6, 3], **kwargs)
    return model

def resnet_gn_mbs101(pretrained=False, **kwargs):
    model = ResNetGnMBS(Bottleneck, [3, 4, 23, 3], **kwargs)
    return model

def resnet_gn_mbs152(pretrained=False, **kwargs):
    model = ResNetGnMBS(Bottleneck, [3, 8, 36, 3], **kwargs)
    return model
