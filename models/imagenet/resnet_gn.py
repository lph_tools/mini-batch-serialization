'''
ResNet model for training
Copyright (c) Sangkug Lym, The University of Texas at Austin, 2018
'''
import torch.nn as nn
import math
from .group_norm  import GroupNorm2d, GroupNorm2dGrp

__all__ = ['resnet_gn18', 'resnet_gn34', 'resnet_gn50', 'resnet_gn101',
           'resnet_gn152']

def conv3x3(in_planes, out_planes, stride=1):
    """3x3 convolution with padding"""
    return nn.Conv2d(in_planes, out_planes, kernel_size=3, stride=stride,
                     padding=1, bias=False)

def gn2d(planes, gn_type, val):
    if gn_type == 'fixed_groups':
        return GroupNorm2dGrp(planes, val, affine=True, track_running_stats=False)
    elif gn_type == 'fixed_channels':
        return GroupNorm2d(planes, val, affine=True, track_running_stats=False)

class BasicBlock(nn.Module):
    expansion = 1

    def __init__(self, inplanes, planes, gn_type, gn_val, stride=1, downsample=None):
        super(BasicBlock, self).__init__()
        self.conv1 = conv3x3(inplanes, planes, stride)
        self.bn1 = gn2d(planes, gn_type, gn_val)
        self.relu = nn.ReLU(inplace=True)
        self.conv2 = conv3x3(planes, planes)
        self.bn2 = gn2d(planes, gn_type, gn_val)
        self.downsample = downsample
        self.stride = stride

    def forward(self, x):
        residual = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)

        if self.downsample is not None:
            residual = self.downsample(x)

        out += residual
        out = self.relu(out)

        return out


class Bottleneck(nn.Module):
    expansion = 4

    def __init__(self, inplanes, planes, gn_type, gn_val, stride=1, downsample=None):
        super(Bottleneck, self).__init__()
        self.conv1 = nn.Conv2d(inplanes, planes, kernel_size=1, bias=False)
        self.bn1 = gn2d(planes, gn_type, gn_val)
        self.conv2 = nn.Conv2d(planes, planes, kernel_size=3, stride=stride,
                               padding=1, bias=False)
        self.bn2 = gn2d(planes, gn_type, gn_val)
        self.conv3 = nn.Conv2d(planes, planes * 4, kernel_size=1, bias=False)
        self.bn3 = gn2d(planes * 4, gn_type, gn_val)
        self.relu = nn.ReLU(inplace=True)
        self.downsample = downsample
        self.stride = stride

    def forward(self, x):
        residual = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)
        out = self.relu(out)

        out = self.conv3(out)
        out = self.bn3(out)

        if self.downsample is not None:
            residual = self.downsample(x)

        out += residual
        out = self.relu(out)

        return out


class ResNetGn(nn.Module):

    def __init__(self, block, layers, num_classes=1000, 
            gn_type='fixed_channels', gn_val=16):
        self.inplanes = 64
        super(ResNetGn, self).__init__()
        self.conv1 = nn.Conv2d(3, 64, kernel_size=7, stride=2, padding=3,
                               bias=False)
        self.bn1 = gn2d(64, gn_type, gn_val)
        self.relu = nn.ReLU(inplace=True)
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        self.layer1 = self._make_layer(block, 64, layers[0], gn_type, gn_val)
        self.layer2 = self._make_layer(block, 128, layers[1], gn_type, gn_val, stride=2)
        self.layer3 = self._make_layer(block, 256, layers[2], gn_type, gn_val, stride=2)
        self.layer4 = self._make_layer(block, 512, layers[3], gn_type, gn_val, stride=2)
        self.avgpool = nn.AvgPool2d(7, stride=1)
        self.fc = nn.Linear(512 * block.expansion, num_classes)

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()
            elif isinstance(m, GroupNorm2d) or isinstance(m, GroupNorm2dGrp):
                m.weight.data.fill_(1)
                m.bias.data.zero_()

        for m in self.modules():
            if isinstance(m, Bottleneck):
                m.bn3.weight.data.fill_(0)
            if isinstance(m, BasicBlock):
                m.bn2.weight.data.fill_(0)


    def _make_layer(self, block, planes, blocks, gn_type, gn_val, stride=1):
        downsample = None
        if stride != 1 or self.inplanes != planes * block.expansion:
            downsample = nn.Sequential(
                nn.Conv2d(self.inplanes, planes * block.expansion,
                          kernel_size=1, stride=stride, bias=False),
                gn2d(planes * block.expansion, gn_type, gn_val),
            )

        layers = []
        layers.append(block(self.inplanes, planes, gn_type, gn_val, stride, downsample))
        self.inplanes = planes * block.expansion
        for i in range(1, blocks):
            layers.append(block(self.inplanes, planes, gn_type, gn_val))

        return nn.Sequential(*layers)

    def forward(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)
        x = self.maxpool(x)

        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)

        x = self.avgpool(x)
        x = x.view(x.size(0), -1)
        x = self.fc(x)

        return x


def resnet_gn18(pretrained=False, **kwargs):
    model = ResNetGn(BasicBlock, [2, 2, 2, 2], **kwargs)
    return model

def resnet_gn34(pretrained=False, **kwargs):
    model = ResNetGn(BasicBlock, [3, 4, 6, 3], **kwargs)
    return model

def resnet_gn50(pretrained=False, **kwargs):
    model = ResNetGn(Bottleneck, [3, 4, 6, 3], **kwargs)
    return model

def resnet_gn101(pretrained=False, **kwargs):
    model = ResNetGn(Bottleneck, [3, 4, 23, 3], **kwargs)
    return model

def resnet_gn152(pretrained=False, **kwargs):
    model = ResNetGn(Bottleneck, [3, 8, 36, 3], **kwargs)
    return model
